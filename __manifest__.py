{
    'name': 'Woo multi almacenes',
    'version': '14.0.1.0.0',
    'category': 'stock',
    'summary': """Se agrega multiples almacenes en woo""",
    'description': """
        Agrega el envio de datos para multiples almacenes en woo, se actualizara el stock por cada almacen
    """,
    'author': 'Prixz',
    'depends': ['odoo_multi_channel_sale', 'woocommerce_odoo_connector', 'stock_nearest_warehouse_acs'],
    'data': [
        'security/ir.model.access.csv',
        'report/report_stock_send_views.xml',
        'data/ir_cron.xml',
        'views/stock_warehouse_views.xml',
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
