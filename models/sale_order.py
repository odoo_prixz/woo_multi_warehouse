from odoo import models


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def get_mapping_products(self):
        products = self.order_line.mapped("product_id.id")
        return self.env["channel.product.mappings"].browse(products)

    def action_confirm(self):
        report_ids = self.generate_report_send_stock()
        res = super(SaleOrder, self.with_context(sale_order=True)).action_confirm()
        """stock_quant = self.env['stock.quant']
        connection = stock_quant._get_woocommerce_connection()
        if connection:
            for record in self:
                mapping = record.get_mapping_products()
                stock_quant.enviar_por_producto(connection, mapping)"""
        report_ids_updated = self.generate_report_send_stock(report_ids)
        for record in self:
            record.update_stock_woo()
        return res

    def update_stock_woo(self):
        stock_quant = self.env["stock.quant"]
        all_products = self.order_line.product_id.channel_mapping_ids
        channel_ids = all_products.mapped("channel_id")
        for channel_id in channel_ids:
            if not channel_id.auto_sync_stock:
                continue
            connection = stock_quant._get_woocommerce_connection(channel_id)
            if connection:
                products = all_products.filtered(lambda x: x.channel_id == channel_id)
                if channel_id.name == "WooCommerce Prixz":
                    stock_quant.enviar_por_producto(connection, products)
                else:
                    stock_quant.enviar_por_producto_no_prixz(channel_id, connection, products)

    def generate_report_send_stock(self, report_ids=None):
        stock_quant = self.env['stock.quant']
        if report_ids:
            for report in report_ids:
                qty = stock_quant._get_available_quantity(
                    report.order_line_id.product_id, report.warehouse_id.lot_stock_id, lot_id=False, strict=False)
                qty_full = stock_quant.get_full_quantity_multi_warehouse(report.order_line_id.product_id)
                report.stock_warehouse_after = qty
                report.stock_total_after = qty_full
            return report_ids
        else:
            report = self.env["report.stock.send"]
            list = []
            for line in self.order_line.filtered(lambda x: x.product_id.type in ['product']):
                qty = stock_quant._get_available_quantity(
                    line.product_id, line.line_warehouse_id.lot_stock_id, lot_id=False, strict=False)
                qty_full = stock_quant.get_full_quantity_multi_warehouse(line.product_id)
                report_id = report.create({
                    "order_id": line.order_id.id,
                    "order_line_id": line.id,
                    "stock_warehouse_before": qty,
                    "stock_total_before": qty_full,
                    "woo_warehouse_at": line.line_warehouse_id.woo_branch,
                    "barcode": line.product_id.barcode
                })
                list.append(report_id)
            return list


