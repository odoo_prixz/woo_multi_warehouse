from odoo import api, fields, models


class StockWarehouse(models.Model):

    _inherit = "stock.warehouse"

    is_multi_stock_woo_activate = fields.Boolean(string="Activar multistock")
    woo_branch = fields.Char(string="Almacén en Woo")
