from odoo import models, _
from woocommerce import API
import logging
import requests

_logger = logging.getLogger(__name__)


class StockQuant(models.Model):
    _inherit = 'stock.quant'

    def get_full_quantity_multi_warehouse(self, product_id):
        warehouse_ids = self.env["stock.warehouse"].search([
            ("is_multi_stock_woo_activate", "=", True),
        ])
        stock_quant = self.env['stock.quant']
        full_quantity = 0
        for warehouse_id in warehouse_ids:
            stock_qty = stock_quant._get_available_quantity(
                product_id, warehouse_id.lot_stock_id, lot_id=False, strict=False)
            full_quantity = full_quantity + stock_qty
        return full_quantity

    def get_meta_quantity_warehouse(self, product_id):
        warehouse_ids = self.env["stock.warehouse"].search([
            ("is_multi_stock_woo_activate", "=", True),
        ])
        meta_data = []
        all_data = {}
        for warehouse_id in warehouse_ids:
            stock_qty = self._get_available_quantity(
                product_id, warehouse_id.lot_stock_id, lot_id=False, strict=False)
            if all_data.get(str(warehouse_id.woo_branch)):
                all_data[str(warehouse_id.woo_branch)] += int(stock_qty)
            else:
                all_data[str(warehouse_id.woo_branch)] = int(stock_qty)
        for key, val in all_data.items():
            data = {
                "key": key,
                "value": val
            }
            meta_data.append(data)
        return meta_data

    def enviar_por_producto(self, connection, all_products):
        for mapping in all_products:
            product_id = mapping.product_name
            full_quantity = self.get_full_quantity_multi_warehouse(product_id)
            meta_data = self.get_meta_quantity_warehouse(product_id)
            # meta_data.append({
            #    "key": "_stock",
            #    "value": int(full_quantity)
            # })
            data = {
                "product_id": f'{mapping.store_product_id}',
                "stock_quantity": int(full_quantity),
                "manage_stock": True,
                "meta_data": meta_data,
                # "stock_status": "instock" if full_quantity > 0 else "outofstock"
            }
            _logger.info(f'*******Envio stock woo*****DATA**** {data}')
            url = f'{connection.url}/wp-json/prixz-pronto/stock/?consumer_key={connection.consumer_key}&consumer_secret={connection.consumer_secret}'
            request_post = requests.put(url, json=data)
            if not request_post.ok and request_post.text:
                _logger.warning("****Proucto Falló: %s \n %s****", str(mapping.product_name.barcode),
                                str(request_post.text))
                res = requests.put(url, json=data)
                _logger.info(f'segundo intento {data}')
                if not res.ok:
                    _logger.info(f'segundo intento  fallo')
                    res = requests.put(url, json=data)
                    _logger.info(f'tercer intento {data}')
                    if not res.ok:
                        _logger.info(f'tercer intento  fallo')
            else:
                _logger.info(f'*******Envio response woo********* {request_post.text}')
                _logger.warning("****Producto correcto: %s \n %s****", str(mapping.product_name.barcode), str(request_post.text))

    def enviar_por_producto_no_prixz(self, channel_id, connection, all_products):
        for mapping in all_products:
            product_id = mapping.product_name
            full_quantity = self.get_full_quantity_multi_warehouse(product_id)
            url = f'products/{mapping.store_product_id}'
            if mapping.store_variant_id != 'No Variants':
                url = f'{url}/variations/{mapping.store_variant_id}'
            res = connection.put(
                url,
                {"stock_quantity": int(full_quantity), "manage_stock": True}
            )
            if res.ok:
                _logger.warning("********Producto correcto: %s ", str(mapping.product_name.barcode))
            else:
                _logger.warning("****Proucto Falló: %s ", str(mapping.product_name.barcode))

    def _cron_update_stock_woo_multi(self):
        channel_ids = self.env['multi.channel.sale'].search([
            ("channel", "=", "woocommerce"),
            ("state", "=", "validate"),
        ])
        for channel_id in channel_ids:
            all_products = self.env["channel.product.mappings"].search([
                ("channel_id", "=", channel_id.id)], order='id desc')
            connection = self._get_woocommerce_connection(channel_id)
            if connection:
                if channel_id.name == "WooCommerce Prixz":
                    self.enviar_por_producto(connection, all_products)
                    _logger.warning("*****FIN_ACTUALIZACION_STOCK*****")
                else:
                    self.enviar_por_producto_no_prixz(channel_id, connection, all_products)
                    _logger.warning("*****FIN_ACTUALIZACION_STOCK_No_PRIXZ*****")

    def _get_woocommerce_connection(self, channel_id):
        if channel_id:
            req = API(
                url=channel_id.woocommerce_url,
                consumer_key=channel_id.woocommerce_consumer_key,
                consumer_secret=channel_id.woocommerce_secret_key,
                #wp_api=True,
                version="wc/v3",
                #query_string_auth=True,
                timeout=999,
            )
            return req
        else:
            _logger.info("No connection channel Woo available")
            return False
