from woocommerce import API
from odoo import models
from odoo.exceptions import UserError
from odoo.tools.translate import _
import requests
import logging
_logger = logging.getLogger(__name__)


class MultiChannelSale(models.Model):
    _inherit = "multi.channel.sale"

    def sync_quantity_woocommerce(self, mapping, qty):
        """Se sobreescribe la funcion original por que no se puede heredar ya que no hay manera de que se
             cambie el body (data) heredando:
             Ahora se recibe en el context el inventario en woo y la cantidad global para mandarlos
             tambien se cambia el endpoint a modificar
        """
        if self.name == "WooCommerce Prixz":
            data = {
                "product_id": f'{mapping.store_product_id}',
                #"stock_quantity": full_quantity,
                "manage_stock": True,
                "meta_data": self.env["stock.quant"].get_meta_quantity_warehouse(mapping.product_name),
                # "stock_status": "instock" if full_quantity > 0 else "outofstock"
            }
            connector = self._get_woocommerce_connection()
            _logger.info(f'*******Envio stock woo*****DATA**** {data}')
            try:
                url = f'{connector.url}/wp-json/prixz-pronto/stock/?consumer_key={connector.consumer_key}&consumer_secret={connector.consumer_secret}'
            except Exception as e:
                _logger.info(f'{e}')
            res = requests.put(url, json=data)
            if not res.ok:
                try:
                    res = res.json()
                    if self.channel_id.debug == "enable":
                        raise UserError(_("Can't update product stock , "+str(res['message'])))
                    _logger.info("Error in updating Product Stock: %r", str(res["message"]))
                    res = requests.put(url, json=data)
                    _logger.info(f'segundo intento {data}')
                    if not res.ok:
                        _logger.info(f'segundo intento  fallo')
                        res = requests.put(url, json=data)
                        _logger.info(f'tercer intento {data}')
                        if not res.ok:
                            _logger.info(f'tercer intento  fallo')
                except Exception as e:
                    _logger.info("Error in updating Product Stock: %r", str(e))
        else:
            if mapping.env.context.get("is_multi_stock_woo_activate"):
                qty = int(mapping.env.context.get("full_quantity"))
            super(MultiChannelSale, self).sync_quantity_woocommerce(mapping, qty)
