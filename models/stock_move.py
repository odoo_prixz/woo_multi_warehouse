from odoo import api, fields, models
import logging
_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    def post_operation(self, stock_operation):
        """Se sobreescribe la funcion original por que no se puede heredar ya que no hay manera de que se
             cambie el body (data) heredando:
             Ahora se manda en el context en el mapping inventario en woo y la cantidad global para mandarlos
        """
        initial_channel_ids = self.env['multi.channel.sale'].search([]).ids
        stock_quant = self.env['stock.quant']
        for move in self:
            is_multi_stock_woo_activate = False
            inventory_branch_woo = ""
            full_quantity = stock_quant.get_full_quantity_multi_warehouse(move.product_id)
            ware_house_picki = move.warehouse_id if move.warehouse_id else move.picking_id.picking_type_id.warehouse_id
            if not ware_house_picki:
                ware_house_picki = self.env["stock.warehouse"].search([
                    ("lot_stock_id", "=", move.location_dest_id.id)
                ], limit=1)
            if not ware_house_picki:
                ware_house_picki = self.env["stock.warehouse"].search([
                    ("lot_stock_id", "=", move.location_id.id)
                ], limit=1)
            if ware_house_picki.is_multi_stock_woo_activate:
                inventory_branch_woo = ware_house_picki.woo_branch
                is_multi_stock_woo_activate = True

            channel_ids = initial_channel_ids.copy()
            product_id = move.product_id
            if move.origin and move.picking_type_id.code == 'outgoing':
                sale_id = self.env['sale.order'].search([('name', '=', move.origin)])
                if sale_id:
                    channel_id = sale_id.channel_mapping_ids.channel_id
                    if channel_id and channel_id.id in channel_ids:
                        channel_ids.remove(channel_id.id)

            if channel_ids:
                source_location_id = move.location_id
                dest_location_id = move.location_dest_id

                for mapping in product_id.channel_mapping_ids:
                    channel_id = mapping.channel_id
                    if not channel_id.auto_sync_stock:
                        continue
                    if channel_id.channel_stock_action == 'qoh':
                        if stock_operation == '_action_confirm' and not move.env.context.get("confirm_sale_order"):
                            continue
                    else:
                        if stock_operation == '_action_done':
                            continue
                    location_ids = channel_id.location_id + channel_id.location_id.child_ids
                    #if source_location_id in location_ids or dest_location_id in location_ids:
                    #qty = channel_id.get_quantity(product_id)
                    qty = stock_quant._get_available_quantity(
                        product_id, ware_house_picki.lot_stock_id, lot_id=False, strict=False)
                    sync_quantity_channel = getattr(channel_id.with_context(
                            inventory_branch_woo=inventory_branch_woo,
                            is_multi_stock_woo_activate=is_multi_stock_woo_activate,
                            full_quantity=full_quantity), 'sync_quantity_%s' % channel_id.channel, None)
                    if sync_quantity_channel:
                        sync_quantity_channel(mapping.with_context(
                            inventory_branch_woo=inventory_branch_woo,
                            is_multi_stock_woo_activate=is_multi_stock_woo_activate,
                            full_quantity=full_quantity), qty)
                    else:
                        _logger.error('Auto Sync Quantity method not found for %s', channel_id.channel)
