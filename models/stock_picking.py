from odoo import fields, models
import logging

_logger = logging.getLogger(__name__)


class StockPicking(models.Model):
    _inherit = "stock.picking"

    def button_validate(self):
        res = super(StockPicking, self).button_validate()
        """if type(res) == type(True) and res:
            for record in self:
                record.send_woo_stock_sync()"""
        return res

    def send_woo_stock_sync(self):
        stock_quant = self.env["stock.quant"]
        all_products = self.move_ids_without_package.product_id.channel_mapping_ids
        channel_ids = all_products.mapped("channel_id")
        for channel_id in channel_ids:
            if not channel_id.auto_sync_stock:
                continue
            connection = stock_quant._get_woocommerce_connection(channel_id)
            if connection:
                products = all_products.filtered(lambda x: x.channel_id == channel_id)
                if channel_id.name == "WooCommerce Prixz":
                    try:
                        stock_quant.enviar_por_producto(connection, products)
                    except Exception as e:
                        _logger.info(f'{e}')
                else:
                    try:
                        stock_quant.enviar_por_producto_no_prixz(channel_id, connection, products)
                    except Exception as e:
                        _logger.info(f'{e}')
