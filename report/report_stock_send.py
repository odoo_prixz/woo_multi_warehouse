from odoo import api, fields, models


class ReportStockSend(models.Model):
    _name = 'report.stock.send'

    stock_warehouse_before = fields.Char(string="Antes de confirmar", )
    stock_warehouse_after = fields.Char(string="Despues de confirmar", )
    stock_total_before = fields.Integer(string="Stock total antes")
    stock_total_after = fields.Integer(string="Stock total despues")
    order_id = fields.Many2one("sale.order", string="Pedido")
    order_line_id = fields.Many2one("sale.order.line", string="Linea")
    warehouse_id = fields.Many2one('stock.warehouse', related="order_line_id.line_warehouse_id",)
    woo_warehouse_at = fields.Char(string="Alamacen woo", )
    name = fields.Char(string="orden", related="order_id.name", )
    barcode = fields.Char(string="EAN")
